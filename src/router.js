import Vue from 'vue'
import Router from 'vue-router'
import Index from './views/Index.vue'
import ShipmentInfo from './components/ShipmentInfo.vue'

Vue.use(Router)

export default new Router({
  mode: 'history',
  base: process.env.BASE_URL,
  routes: [
    {
      path: '/',
      name: 'index',
      component: Index
    },
    {
      path: '/:id',
      name: 'shipment',
      component: ShipmentInfo,
      props: true
    }
  ]
})
