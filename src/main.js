import Vue from 'vue'
import router from './router'
import axios from 'axios'
import VueAxios from 'vue-axios'
import VueLocalStorage from 'vue-localstorage'
import Toast from 'vue-easy-toast'
import App from './App.vue'

Vue.use(VueAxios, axios)
Vue.use(VueLocalStorage)
Vue.use(Toast)

Vue.filter('highlight', (words, query) => {
  const searchQueryFormatted = new RegExp(query, 'ig')
  return words.toString().replace(searchQueryFormatted, (matchedTxt) => {
    return `<span class="highlighter">${matchedTxt}</span>`
  })
})

Vue.config.productionTip = false

new Vue({
  router,
  render: h => h(App),
}).$mount('#app')
