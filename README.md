# SpaceX Cargo Planner
This app is for shipment cargo space calculating. It loads part of S&P500 companies from external source (if local copy is not present). Calculates the needed cargo bays count for selected shipment.

User can search company in the list, edit shipments slot data, save shipments list to local storage of browser, load shipment companies

![App preview](public/preview.png)

## TODO
- ~~MVP~~
- ~~search company by title~~
- ~~cargo boxes logic + validation~~
- ~~add some toast messages~~
- ~~responsive~~
- ~~final styling and CSS prefixes~~
- ~~code cleanup and W3C validation~~
- ~~update README~~

## Project setup
```
git clone git@bitbucket.org:Refleckt/spacex-cargo-planner.git
```

```
cd spacex-cargo-planner
```

Then run (you can use npm instead of yarn)
```
yarn install
```

### Compiles and hot-reloads for development
```
yarn run serve
```

### Compiles and minifies for production
```
yarn run build
```

### Lints and fixes files
```
yarn run lint
```
